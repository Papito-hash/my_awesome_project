import unittest
from app.model import Usuario


class TestModelUsuario(unittest.TestCase):
    def test_table_name_should_return_usuario(self):
        expected = "Usuario"
        result = Usuario(nome="Felipe", sobrenome="Silva")

        self.assertEqual(expected, result.__tablename__)

    def test_table_repr_should_return_user(self):
        expected = "Usuario(nome='felipe', sobrenome='silva')"
        result = Usuario(nome="felipe", sobrenome="silva")

        self.assertEqual(expected, result.__repr__())
