import flask
import wtforms
from .model import session, Usuario

crud_bp = flask.Blueprint("crud", __name__)


class User(wtforms.Form):
    nome = wtforms.StringField("Nome")
    sobrenome = wtforms.StringField("Sobrenome")
    butt = wtforms.SubmitField("OK!")


@crud_bp.route("/")
def root():
    return flask.render_template("home.html", form=User())


@crud_bp.route("/get-users")
def get_users():
    users = [u.as_dict() for u in session.query(Usuario).all()]
    resp = flask.make_response(flask.jsonify(users))
    resp.headers["Access-Control-Allow-Origin"] = "*"
    return resp


@crud_bp.route("/post-user", methods=("POST",))
def post_user():
    user = flask.request.get_json(force=True)
    db_user = Usuario(**user)
    session.add(db_user)
    session.commit()
    return "ok", 201
