import flask
from .crud import crud_bp
from .model import session


app = flask.Flask(__name__)
app.register_blueprint(crud_bp, url_prefix="/")


@app.teardown_request
def remove_session(ex=None):
    session.remove()
